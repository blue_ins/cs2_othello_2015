#include "player.h"
#include <stdio.h>
#include <vector>
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     
     board = Board();
     team = side; 
}

/*
 * Destructor for the player.
 */
 
Player::~Player() {
}

/*Move *Player::find_best_move(Board * board)
{
	for (int i = 0; i < 8; i++)
    {
		for (int j = 0; j < 8; j++)
		{
			m = new Move(i, j);
			if (board->checkMove(m, team))
			{
				move_array.push_back(m);
			}
		}
	}
	if (move_array.size() == 0)
	{
		return NULL;
	}
	best_move = move_array[0];
	board->doMove(move_array[0], team);
	best_points = board->count(team) - board->count(other_team);
	for (int a = 1; a < (int) move_array.size(); a++)
	{
		copy_board = board.copy();
		copy_board->doMove(move_array[a], team);
		points = copy_board->count(team) - copy_board->count(other_team);
		if (move_array[a]->x == 0 || move_array[a]->y == 0 || 
		move_array[a]->y == 7 || move_array[a]->y == 7)
		{
			points = 3*points;
		}
		if (move_array[a]->x == 1 || move_array[a]->y == 1 ||
		move_array[a]->x == 6 || move_array[a]->y == 6)
		{
			points = points - 9;
		}
		if (points > best_points)
		{
			best_points = points;
			best_move = move_array[a];
		}
	}
*/
/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
 
Move *Player::doMove(Move *opponentsMove, int msLeft) {

	// Commented codes: the previous versions of codes that we had
	// to write
    /*Side other_team;
    if (team == BLACK)
    {
		other_team = WHITE;
	}
	else
	{
		other_team = BLACK;
	}
	Move *m;
    board.doMove(opponentsMove, other_team);
    for (int i = 0; i < 8; i++)
    {
		for (int j = 0; j < 8; j++)
		{
			m = new Move(i, j);
			if (board.checkMove(m, team))
			{
				board.doMove(m, team);
				return m;
			}
		}
	}*/
	/*
	Side other_team;
    if (team == BLACK)
    {
		other_team = WHITE;
	}
	else
	{
		other_team = BLACK;
	}
	
	Move *m;
	Move *best_move;
	std::vector<Move*> move_array;
	int points;
	int best_points;
	Board *copy_board;
    board.doMove(opponentsMove, other_team);
    
    
    for (int i = 0; i < 8; i++)
    {
		for (int j = 0; j < 8; j++)
		{
			m = new Move(i, j);
			if (board.checkMove(m, team))
			{
				move_array.push_back(m);
			}
		}
	}
	if (move_array.size() == 0)
	{
		return NULL;
	}
	best_move = move_array[0];
	copy_board = board.copy();
	copy_board->doMove(move_array[0], team);
	best_points = copy_board->count(team) - copy_board->count(other_team);
	for (int a = 1; a < (int) move_array.size(); a++)
	{
		copy_board = board.copy();
		copy_board->doMove(move_array[a], team);
		points = copy_board->count(team) - copy_board->count(other_team);
		if (move_array[a]->x == 0 || move_array[a]->y == 0 || 
		move_array[a]->y == 7 || move_array[a]->y == 7)
		{
			fprintf(stderr, "edge\n");
			points = 3*points;
		}
		if ((move_array[a]->x == 0 && move_array[a]->y == 0) || 
		(move_array[a]->x == 0 && move_array[a]->y == 7) ||
		(move_array[a]->x == 7 && move_array[a]->y == 0) ||
		(move_array[a]->x == 7 && move_array[a]->y == 7))
		{
			fprintf(stderr, "corner\n");
			points = points + 100;
		}

		if (move_array[a]->x == 1 || move_array[a]->y == 1 ||
		move_array[a]->x == 6 || move_array[a]->y == 6)
		{
			fprintf(stderr, "next to corner\n");
			points = points - 9;
		}
		if (points > best_points)
		{
			best_points = points;
			best_move = move_array[a];
		}
	}
	fprintf(stderr, "end turn\n");
	board.doMove(best_move, team);
    return best_move;
   */
    /*
	Side other_team;
    if (team == BLACK)
    {
		other_team = WHITE;
	}
	else
	{
		other_team = BLACK;
	}
	
	Move *m;
	Move *best_move;
	std::vector<Move*> move_array;
	int points;
	int best_points;
	Board *copy_board;
    board.doMove(opponentsMove, other_team);
    
    
    for (int i = 0; i < 8; i++)
    {
		for (int j = 0; j < 8; j++)
		{
			m = new Move(i, j);
			if (board.checkMove(m, team))
			{
				move_array.push_back(m);
			}
		}
	}
	if (move_array.size() == 0)
	{
		return NULL;
	}
	best_move = move_array[0];
	copy_board = board.copy();
	copy_board->doMove(move_array[0], team);
	best_points = copy_board->count(team) - copy_board->count(other_team);
	for (int a = 1; a < (int) move_array.size(); a++)
	{
		copy_board = board.copy();
		copy_board->doMove(move_array[a], team);
		points = copy_board->count(team) - copy_board->count(other_team);
		if (move_array[a]->x == 0)
		{
			fprintf(stderr, "edge\n");
			points = points + 5;
		}
		if (move_array[a]->y == 0)
		{
			fprintf(stderr, "edge\n");
			points = points + 5;
		}
		if (move_array[a]->y == 7)
		{
			fprintf(stderr, "edge\n");
			points = points + 5;
		}
		if (move_array[a]->y == 7)
		{
			fprintf(stderr, "edge\n");
			points = points + 5;
		}
		if ((move_array[a]->x == 0 && move_array[a]->y == 0) || 
		(move_array[a]->x == 0 && move_array[a]->y == 7) ||
		(move_array[a]->x == 7 && move_array[a]->y == 0) ||
		(move_array[a]->x == 7 && move_array[a]->y == 7))
		{
			fprintf(stderr, "corner\n");
			points = points + 100;
		}
		if (move_array[a]->x == 1)
		{
			fprintf(stderr, "next to corner\n");
			points = points - 9;
		}
		if (move_array[a]->y == 1)
		{
			fprintf(stderr, "next to corner\n");
			points = points - 9;
		}
		if (move_array[a]->x == 6)
		{
			fprintf(stderr, "next to corner\n");
			points = points - 9;
		}
		if (move_array[a]->y == 6)
		{
			fprintf(stderr, "next to corner\n");
			points = points - 9;
		}

		
		
		
		if ((move_array[a]->x == 0) && (move_array[a]->y == 1))
		{
			fprintf(stderr, "next to corner\n");
			points = points - 10;
		}
		if ((move_array[a]->x == 1) && (move_array[a]->y == 0))
		{
			fprintf(stderr, "next to corner\n");
			points = points - 10;
		}
		if ((move_array[a]->x == 1) && (move_array[a]->y == 1))
		{
			fprintf(stderr, "next to corner\n");
			points = points - 10;
		}
		if ((move_array[a]->x == 7) && (move_array[a]->y == 6))
		{
			fprintf(stderr, "next to corner\n");
			points = points - 10;
		}
		if ((move_array[a]->x == 6) && (move_array[a]->y == 7))
		{
			fprintf(stderr, "next to corner\n");
			points = points - 10;
		}
		if ((move_array[a]->x == 6) && (move_array[a]->y == 6))
		{
			fprintf(stderr, "next to corner\n");
			points = points - 10;
		}
		if ((move_array[a]->x == 1) && (move_array[a]->y == 6))
		{
			fprintf(stderr, "next to corner\n");
			points = points - 10;
		}
		if ((move_array[a]->x == 1) && (move_array[a]->y == 7))
		{
			fprintf(stderr, "next to corner\n");
			points = points - 10;
		}
		if ((move_array[a]->x == 0) && (move_array[a]->y == 6))
		{
			fprintf(stderr, "next to corner\n");
			points = points - 10;
		}
		if ((move_array[a]->x == 6) && (move_array[a]->y == 1))
		{
			fprintf(stderr, "next to corner\n");
			points = points - 10;
		}
		if ((move_array[a]->x == 7) && (move_array[a]->y == 1))
		{
			fprintf(stderr, "next to corner\n");
			points = points - 10;
		}
		if ((move_array[a]->x == 6) && (move_array[a]->y == 0))
		{
			fprintf(stderr, "next to corner\n");
			points = points - 10;
		}
		if (points > best_points)
		{
			best_points = points;
			best_move = move_array[a];
		}
	}
	fprintf(stderr, "end turn\n");
	board.doMove(best_move, team);
    return best_move;
 
	*/
	Side other_team;
    if (team == BLACK)
    {
		other_team = WHITE;
	}
	else
	{
		other_team = BLACK;
	}
	
	Move *m;
	Move *best_move;
	std::vector<Move*> move_array;
	int points;
	int best_points;
	Board *copy_board;
    board.doMove(opponentsMove, other_team);
    
    
    for (int i = 0; i < 8; i++)
    {
		for (int j = 0; j < 8; j++)
		{
			m = new Move(i, j);
			if (board.checkMove(m, team))
			{
				move_array.push_back(m);
			}
		}
	}
	if (move_array.size() == 0)
	{
		// return NULL if no possible moves are there.
		return NULL;
	}
	best_move = move_array[0];
	copy_board = board.copy();
	copy_board->doMove(move_array[0], team);
	best_points = copy_board->count(team) - copy_board->count(other_team);
	for (int a = 1; a < (int) move_array.size(); a++)
	{
		copy_board = board.copy();
		copy_board->doMove(move_array[a], team);
		points = copy_board->count(team) - copy_board->count(other_team);
		// These if statements give bonus points if they are on the
		// edge or at the corner, and negative points if they are
		// next to corner.
		
		if (move_array[a]->x == 0)
		{
			points = points + 5;
		}
		if (move_array[a]->y == 0)
		{
			points = points + 5;
		}
		if (move_array[a]->y == 7)
		{
			points = points + 5;
		}
		if (move_array[a]->y == 7)
		{
			points = points + 5;
		}
		
		if ((move_array[a]->x == 0 && move_array[a]->y == 0) || 
		(move_array[a]->x == 0 && move_array[a]->y == 7) ||
		(move_array[a]->x == 7 && move_array[a]->y == 0) ||
		(move_array[a]->x == 7 && move_array[a]->y == 7))
		{
			points = points + 100;
		}
		if (move_array[a]->x == 1)
		{
			points = points - 9;
		}
		if (move_array[a]->y == 1)
		{
			points = points - 9;
		}
		if (move_array[a]->x == 6)
		{
			points = points - 9;
		}
		if (move_array[a]->y == 6)
		{
			points = points - 9;
		}
		if ((move_array[a]->x == 0) && (move_array[a]->y == 1))
		{
			points = points - 10;
		}
		if ((move_array[a]->x == 1) && (move_array[a]->y == 0))
		{
			points = points - 15;
		}
		if ((move_array[a]->x == 1) && (move_array[a]->y == 1))
		{
			points = points - 15;
		}
		if ((move_array[a]->x == 7) && (move_array[a]->y == 6))
		{
			points = points - 15;
		}
		if ((move_array[a]->x == 6) && (move_array[a]->y == 7))
		{
			points = points - 15;
		}
		if ((move_array[a]->x == 6) && (move_array[a]->y == 6))
		{
			points = points - 15;
		}
		if ((move_array[a]->x == 1) && (move_array[a]->y == 6))
		{
			points = points - 15;
		}
		if ((move_array[a]->x == 1) && (move_array[a]->y == 7))
		{
			points = points - 15;
		}
		if ((move_array[a]->x == 0) && (move_array[a]->y == 6))
		{
			points = points - 15;
		}
		if ((move_array[a]->x == 6) && (move_array[a]->y == 1))
		{
			points = points - 15;
		}
		if ((move_array[a]->x == 7) && (move_array[a]->y == 1))
		{
			points = points - 15;
		}
		if ((move_array[a]->x == 6) && (move_array[a]->y == 0))
		{
			points = points - 15;
		}
		if (points > best_points)
		{
			best_points = points;
			best_move = move_array[a];
		}
	}
	board.doMove(best_move, team);
    return best_move;
}
